package com.atlassian.jira.extra.icalfeed.service;

import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.extra.icalfeed.dateprovider.DateProvider;
import com.atlassian.jira.extra.icalfeed.util.QueryUtil;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.index.DocumentConstants;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.query.Query;
import com.google.common.collect.ImmutableList;
import org.joda.time.DateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultEntityAsServiceTestCase
{
    public static final String START_FIELD_KEY = "something1";
    public static final String END_FIELD_KEY = "something2";
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private FieldManager fieldManager;

    @Mock
    private PluginAccessor pluginAccessor;

    @Mock
    private SearchService searchService;

    @Mock
    private DateProvider dateProvider;

    @Mock
    private QueryUtil queryUtil;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private Field field;

    private DefaultEntityAsEventService defaultEntityAsEventService;
    private Issue issue;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);

        when(fieldManager.getField(anyString())).thenReturn(field);
        when(applicationProperties.getDefaultBackedString(APKeys.JIRA_SEARCH_VIEWS_DEFAULT_MAX)).thenReturn("1000");

        issue = getIssue();
        defaultEntityAsEventService = new DefaultEntityAsEventService(jiraAuthenticationContext, fieldManager, null, null, pluginAccessor, searchService, queryUtil, applicationProperties)
        {
            @Override
            Collection<DateProvider> getDateProvider(Field field)
            {
                return Arrays.asList(dateProvider);
            }
        };
    }

    @After
    public void tearDown() throws Exception
    {
        jiraAuthenticationContext = null;
        fieldManager = null;
        pluginAccessor = null;
        searchService = null;
        queryUtil = null;
        applicationProperties = null;
        dateProvider = null;
        field = null;
    }

    @Test
    public void testVersionWithoutReleaseDatesNotReturnedInResults() throws SearchException, ParseException
    {
        DateTime startDate = new DateTime();
        when(dateProvider.getStart(same(issue), Matchers.<Field>anyObject())).thenReturn(startDate);
        when(dateProvider.getEnd(same(issue), Matchers.<Field>anyObject(), same(startDate))).thenReturn(startDate.plusMinutes(30));

        when(searchService.search(Matchers.<ApplicationUser>anyObject(), Matchers.<Query>anyObject(), Matchers.<PagerFilter>anyObject())).thenReturn(
                new SearchResults(Arrays.asList(issue), 1, PagerFilter.getUnlimitedFilter().getMax(), 0)
        );

        Project aProject = mock(Project.class);
        Version versionWithoutReleaseDate = mock(Version.class);
        when(aProject.getVersions()).thenReturn(Arrays.asList(versionWithoutReleaseDate));

        when(queryUtil.getBrowseableProjectsFromQuery(Matchers.<ApplicationUser>anyObject(), Matchers.<Query>anyObject())).thenReturn(
                new HashSet<Project>(Arrays.asList(aProject))
        );

        EntityAsEventService.Result result = defaultEntityAsEventService.search(null, new HashSet<String>(Arrays.asList(DocumentConstants.ISSUE_DUEDATE)), null, true, null, 0, 0, 0);

        assertNotNull(result);
        assertNotNull(result.issues);
        assertFalse(result.issues.isEmpty());

        assertNotNull(result.fixedVersions);
        assertTrue(result.fixedVersions.isEmpty());

        verify(versionWithoutReleaseDate, atLeastOnce()).getReleaseDate();
    }

    @Test
    public void testVersionReturnedInResultsEvenIfNoIssueIsAssociatedWithIt() throws SearchException, ParseException
    {
        DateTime startDate = new DateTime();
        when(dateProvider.getStart(same(issue), Matchers.<Field>anyObject())).thenReturn(startDate);
        when(dateProvider.getEnd(same(issue), Matchers.<Field>anyObject(), same(startDate))).thenReturn(startDate.plusMinutes(30));

        when(searchService.search(Matchers.<ApplicationUser>anyObject(), Matchers.<Query>anyObject(), Matchers.<PagerFilter>anyObject())).thenReturn(
                new SearchResults(Collections.<Issue>emptyList(), 0, PagerFilter.getUnlimitedFilter().getMax(), 0)
        );

        Project aProject = mock(Project.class);
        Version versionWithoutReleaseDate = mock(Version.class);
        Version version = mock(Version.class);
        when(version.getReleaseDate()).thenReturn(new Date());
        when(aProject.getVersions()).thenReturn(Arrays.asList(versionWithoutReleaseDate, version));

        when(queryUtil.getBrowseableProjectsFromQuery(Matchers.<ApplicationUser>anyObject(), Matchers.<Query>anyObject())).thenReturn(
                new HashSet<Project>(Arrays.asList(aProject))
        );

        EntityAsEventService.Result result = defaultEntityAsEventService.search(null, new HashSet<String>(Arrays.asList(DocumentConstants.ISSUE_DUEDATE)), null, true, null, 0, 0, 0);

        assertNotNull(result);
        assertNotNull(result.issues);
        assertTrue(result.issues.isEmpty());

        assertNotNull(result.fixedVersions);
        assertFalse(result.fixedVersions.isEmpty());
    }

    @Test
    public void shouldAddStartDateAndEndDateInResults() throws SearchException, ParseException {
        // arrange
        when(searchService.search(any(ApplicationUser.class), any(Query.class), any(PagerFilter.class)))
                .thenReturn(new SearchResults(Arrays.asList(issue), 1, PagerFilter.getUnlimitedFilter().getMax(), 0));

        Field startField = mock(Field.class);
        when(startField.getId()).thenReturn(START_FIELD_KEY);
        Field endField = mock(Field.class);
        when(endField.getId()).thenReturn(END_FIELD_KEY);
        FieldManager fieldManager = getFieldManager(startField, endField);

        DateTime startDate = new DateTime(2016, 4, 13, 0, 0, 0);
        DateTime endDate = new DateTime(2016, 4, 14, 0, 0, 0);
        Collection<DateProvider> builtInProviders = getDateProviders(startField, endField, startDate, endDate);

        ImmutableList<EntityAsEventService.Duration> durations = getDurations();

        DefaultEntityAsEventService defaultEntityService = new DefaultEntityAsEventService(jiraAuthenticationContext, fieldManager, builtInProviders, null, null, searchService, queryUtil, null);

        // act
        EntityAsEventService.Result result = defaultEntityService.search(null, null, durations, false, null, 0, 0, 1);

        // assert
        EntityAsEventService.IssueDateResult issueDateResult = result.issues.stream().findFirst().get();
        assertThat(issueDateResult.start, is(startDate));
        assertThat(issueDateResult.end, is(endDate));
    }

    @Test
    public void shouldAddStartDateAndReinterpretedEndDateInResultsIfStartFieldIsCreatedAndEndFieldIsDueDate() throws SearchException, ParseException {
        // arrange
        when(searchService.search(any(ApplicationUser.class), any(Query.class), any(PagerFilter.class)))
                .thenReturn(new SearchResults(Arrays.asList(issue), 1, PagerFilter.getUnlimitedFilter().getMax(), 0));

        Field startField = mock(Field.class);
        when(startField.getId()).thenReturn(IssueFieldConstants.CREATED);
        Field endField = mock(Field.class);
        when(endField.getId()).thenReturn(IssueFieldConstants.DUE_DATE);
        FieldManager fieldManager = getFieldManager(startField, endField);

        DateTime startDate = new DateTime(2016, 4, 13, 1, 0, 0);
        DateTime endDate = new DateTime(2016, 4, 13, 0, 0, 0);
        Collection<DateProvider> builtInProviders = getDateProviders(startField, endField, startDate, endDate);

        ImmutableList<EntityAsEventService.Duration> durations = getDurations();

        DefaultEntityAsEventService defaultEntityService = new DefaultEntityAsEventService(jiraAuthenticationContext, fieldManager, builtInProviders, null, null, searchService, queryUtil, null);

        // act
        EntityAsEventService.Result result = defaultEntityService.search(null, null, durations, false, null, 0, 0, 1);

        // assert
        DateTime reinterpretedEndDate = new DateTime(2016, 4, 13, 23, 59, 59, 999);
        EntityAsEventService.IssueDateResult issueDateResult = result.issues.stream().findFirst().get();
        assertThat(issueDateResult.start, is(startDate));
        assertThat(issueDateResult.end, is(reinterpretedEndDate));
    }

    private Issue getIssue() {
        Issue issue = mock(Issue.class);
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());

        when(issue.getCreated()).thenReturn(currentTime);
        when(issue.getUpdated()).thenReturn(currentTime);

        IssueType issueType = mock(IssueType.class);
        when(issueType.getNameTranslation(Matchers.<I18nHelper>anyObject())).thenReturn("");
        when(issue.getIssueTypeObject()).thenReturn(issueType);

        Status issueStatus = mock(Status.class);
        when(issueStatus.getNameTranslation(Matchers.<I18nHelper>anyObject())).thenReturn("");
        when(issue.getStatusObject()).thenReturn(issueStatus);
        return issue;
    }

    private ImmutableList<EntityAsEventService.Duration> getDurations() {
        EntityAsEventService.Duration duration = mock(EntityAsEventService.Duration.class);
        when(duration.getStartDateFieldKey()).thenReturn(START_FIELD_KEY);
        when(duration.getEndDateFieldKey()).thenReturn(END_FIELD_KEY);
        return ImmutableList.of(duration);
    }

    private FieldManager getFieldManager(Field startField, Field endField) {
        FieldManager fieldManager = mock(FieldManager.class);
        when(fieldManager.getField(START_FIELD_KEY)).thenReturn(startField);
        when(fieldManager.getField(END_FIELD_KEY)).thenReturn(endField);
        return fieldManager;
    }

    private Collection<DateProvider> getDateProviders(Field startField, Field endField, DateTime startDate, DateTime endDate) {
        DateProvider startDateProvider = mock(DateProvider.class);
        when(startDateProvider.handles(startField)).thenReturn(true);
        when(startDateProvider.getStart(issue, startField)).thenReturn(startDate);

        DateProvider endDateProvider = mock(DateProvider.class);
        when(endDateProvider.handles(endField)).thenReturn(true);
        when(endDateProvider.getStart(issue, endField)).thenReturn(endDate);

        return ImmutableList.of(startDateProvider, endDateProvider);
    }
}

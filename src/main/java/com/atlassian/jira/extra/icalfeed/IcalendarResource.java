package com.atlassian.jira.extra.icalfeed;


import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.filter.SearchRequestService;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.pico.ComponentManager;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.event.mau.MauApplicationKey;
import com.atlassian.jira.event.mau.MauEventService;
import com.atlassian.jira.extra.icalfeed.service.EntityAsEventService;
import com.atlassian.jira.extra.icalfeed.util.QueryUtil;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.customfields.impl.DateCFType;
import com.atlassian.jira.issue.customfields.impl.DateTimeCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.sharing.search.SharedEntitySearchParametersBuilder;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.web.component.jql.AutoCompleteJsonGenerator;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.query.Query;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.extensions.property.WrCalDesc;
import net.fortuna.ical4j.extensions.property.WrCalName;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.ComponentList;
import net.fortuna.ical4j.model.ParameterList;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.PropertyList;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.ValidationException;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.parameter.XParameter;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.Created;
import net.fortuna.ical4j.model.property.Description;
import net.fortuna.ical4j.model.property.DtEnd;
import net.fortuna.ical4j.model.property.DtStamp;
import net.fortuna.ical4j.model.property.DtStart;
import net.fortuna.ical4j.model.property.LastModified;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Summary;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.model.property.XProperty;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

@Path("ical")
@AnonymousAllowed
public class IcalendarResource
{
    private static final Logger LOG = LoggerFactory.getLogger(IcalendarResource.class);

    private static final Collection<String> BUILT_IN_SYSTEM_FIELD_KEYS = Collections.unmodifiableSet(
            new HashSet<String>(
                    Arrays.asList(
                            IssueFieldConstants.CREATED,
                            IssueFieldConstants.UPDATED,
                            IssueFieldConstants.DUE_DATE,
                            IssueFieldConstants.RESOLUTION
                    )
            )
    );

    private String CALENDAR_PRODUCT_ID = "-//Atlassian JIRA//iCalendar Plugin 1.0//EN";

    private static final String X_PROP_ISSUE_DATE_FIELD_NAME = "X-JIRA-ISSUE-DATE-FIELD";

    private static final String X_PARAM_ISSUE_DATE_FIELD_KEY = "X-JIRA-ISSUE-DATE-FIELD-KEY";

    private static final String X_PROP_ISSUE_END_DATE_FIELD_NAME = "X-JIRA-ISSUE-END-DATE-FIELD";

    private static final String X_PARAM_ISSUE_END_DATE_FIELD_KEY = "X-JIRA-ISSUE-END-DATE-FIELD-KEY";

    private static final String X_PARAM_UID_STATIC = "X-JIRA-UID-STATIC";

    private static final String X_PROJECT = "X-JIRA-PROJECT";

    private static final String X_PROJECT_KEY = "X-JIRA-PROJECT-KEY";

    private static final String X_PROJECT_ID = "X-JIRA-PROJECT-ID";

    private static final String X_ISSUE_KEY = "X-JIRA-ISSUE-KEY";

    private static final String X_ISSUE_STATUS = "X-JIRA-STATUS";

    private static final String X_ISSUE_RESOLUTION = "X-JIRA-RESOLUTION";

    private static final String X_ASSIGNEE = "X-JIRA-ASSIGNEE";

    private static final String X_ASSIGNEE_ID = "X-JIRA-ASSIGNEE-ID";

    private static final String X_VERSION_ID = "X-JIRA-VERSION-ID";

    private static final String X_VERSION_RELEASED = "X-JIRA-VERSION-RELEASED";

    private final ApplicationProperties applicationProperties;

    private final JiraAuthenticationContext jiraAuthenticationContext;

    private final EntityAsEventService entityAsEventService;

    private final SearchService searchService;

    private final TimeZoneRegistry timeZoneRegistry;

    private final FieldManager fieldManager;

    private final CustomFieldManager customFieldManager;

    private final QueryUtil queryUtil;

    private final ProjectManager projectManager;

    private final PermissionManager permissionManager;

    private final SearchRequestService searchRequestService;

    private final AutoCompleteJsonGenerator autoCompleteJsonGenerator;

    private final MauEventService mauEventService;

    public IcalendarResource(ApplicationProperties applicationProperties,
                             JiraAuthenticationContext jiraAuthenticationContext,
                             EntityAsEventService entityAsEventService,
                             SearchService searchService,
                             FieldManager fieldManager,
                             CustomFieldManager customFieldManager,
                             QueryUtil queryUtil,
                             ProjectManager projectManager,
                             PermissionManager permissionManager,
                             SearchRequestService searchRequestService,
                             MauEventService mauEventService)
    {
        this.applicationProperties = applicationProperties;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.entityAsEventService = entityAsEventService;
        this.searchService = searchService;
        this.fieldManager = fieldManager;
        this.customFieldManager = customFieldManager;
        this.queryUtil = queryUtil;
        this.projectManager = projectManager;
        this.permissionManager = permissionManager;
        this.searchRequestService = searchRequestService;
        this.autoCompleteJsonGenerator = ComponentManager.getInstance().getComponentInstanceOfType(AutoCompleteJsonGenerator.class);
        this.mauEventService = mauEventService;

        timeZoneRegistry = TimeZoneRegistryFactory.getInstance().createRegistry();
    }

    @Path("project/{projectKey}/events.ics")
    @GET
    @Produces("text/calendar")
    public Response getIcalendarByProject(
            @PathParam("projectKey") String projectKey,
            @QueryParam("dateFieldName") List<String> dateFieldNames,
            @QueryParam("duration") List<String> durations,
            @QueryParam("includeFixVersions") boolean includeFixVersions,
            @DefaultValue("0") @QueryParam("start") long start,
            @DefaultValue("0") @QueryParam("end") long end,
            @DefaultValue("0") @QueryParam("maxIssue") int maxIssue)
    {
        try
        {
            if (StringUtils.isBlank(projectKey))
                return Response.status(Response.Status.BAD_REQUEST).build();

            mauEventService.setApplicationForThreadBasedOnProject(projectManager.getProjectByCurrentKeyIgnoreCase(projectKey));
            return createIcalendarResponse(
                    search(JqlQueryBuilder.newBuilder().where().project(projectKey).buildQuery(), dateFieldNames, durations, includeFixVersions, jiraAuthenticationContext.getLoggedInUser(), start, end, maxIssue)
            );
        }
        catch (Exception generalError)
        {
            LOG.error("Unable to export issues to iCalendar", generalError);
            return Response.serverError().build();
        }
    }

    @Path("search/jql/events.ics")
    @GET
    @Produces("text/calendar")
    public Response getIcalendarByJql(
            @QueryParam("jql") String jql,
            @QueryParam("dateFieldName") List<String> dateFieldNames,
            @QueryParam("duration") List<String> durations,
            @QueryParam("includeFixVersions") boolean includeFixVersions,
            @DefaultValue("0") @QueryParam("start") long start,
            @DefaultValue("0") @QueryParam("end") long end,
            @DefaultValue("0") @QueryParam("maxIssue") int maxIssue)
    {
        try
        {
            if (StringUtils.isBlank(jql))
                return Response.status(Response.Status.BAD_REQUEST).build();

            ApplicationUser loggedInUser = jiraAuthenticationContext.getLoggedInUser();
            SearchService.ParseResult jqlParseResult = searchService.parseQuery(loggedInUser, StringUtils.defaultString(jql));

            if(!jqlParseResult.isValid())
            {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }

            Query query = jqlParseResult.getQuery();
            MessageSet messageSet =  searchService.validateQuery(loggedInUser, query);
            if(messageSet.hasAnyErrors())
            {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
            else
            {
                mauEventService.setApplicationForThread(MauApplicationKey.family());
                return createIcalendarResponse(search(query, dateFieldNames, durations, includeFixVersions, loggedInUser, start, end, maxIssue));
            }
        }
        catch (Exception generalError)
        {
            LOG.error("Unable to export issues to iCalendar", generalError);
            return Response.serverError().build();
        }
    }

    @Path("search/filter/events.ics")
    @GET
    @Produces("text/calendar")
    public Response getIcalendarByJql(
            @QueryParam("searchFilterId") long searchFilterId,
            @QueryParam("dateFieldName") List<String> dateFieldNames,
            @QueryParam("duration") List<String> durations,
            @QueryParam("includeFixVersions") boolean includeFixVersions,
            @DefaultValue("0") @QueryParam("start") long start,
            @DefaultValue("0") @QueryParam("end") long end,
            @DefaultValue("0") @QueryParam("maxIssue") int maxIssue)
    {
        try
        {
            ApplicationUser loggedInUser = jiraAuthenticationContext.getLoggedInUser();
            SearchRequest searchRequest = searchRequestService.getFilter(new JiraServiceContextImpl(loggedInUser), searchFilterId);

            if (null == searchRequest)
                return Response.status(Response.Status.BAD_REQUEST).build();

            mauEventService.setApplicationForThread(MauApplicationKey.family());
            return createIcalendarResponse(search(searchRequest.getQuery(), dateFieldNames, durations, includeFixVersions, loggedInUser, start, end, maxIssue));
        }
        catch (Exception generalError)
        {
            LOG.error("Unable to export issues to iCalendar", generalError);
            return Response.serverError().build();
        }
    }

    private Response createIcalendarResponse(Calendar calendar) throws IOException, ValidationException
    {
        Writer iCalendarWriter = new StringWriter();
        new CalendarOutputter(false).output(calendar, iCalendarWriter);

        return Response.ok(iCalendarWriter.toString()).build();
    }

    private Calendar search(Query query, Collection<String> dateFieldNames, Collection<String> durations, boolean includeFixVersions, ApplicationUser user, long start, long end, int maxIssue) throws SearchException, ParseException, MalformedURLException, URISyntaxException
    {
        return toIcalendar(
                entityAsEventService.search(
                        query,
                        dateFieldNames,
                        Collections2.filter(
                                Collections2.transform(
                                        null == durations ? Collections.<String>emptySet() : durations,
                                        new DurationQueryParamsToDurationsTransformFunction()
                                ),
                                Predicates.notNull()
                        ),
                        includeFixVersions,
                        user,
                        start,
                        end,
                        maxIssue
                )
        );
    }

    private Calendar toIcalendar(EntityAsEventService.Result result)
            throws ParseException, MalformedURLException, URISyntaxException
    {
        ComponentList iCalendarComponents = new ComponentList();

        TimeZone timeZone = timeZoneRegistry.getTimeZone(TimeZone.getDefault().getID());
        VTimeZone vTimeZone = timeZone.getVTimeZone();
        iCalendarComponents.add(vTimeZone);

        PropertyList iCalendarProps = new PropertyList();
        iCalendarProps.add(new ProdId(CALENDAR_PRODUCT_ID));
        iCalendarProps.add(net.fortuna.ical4j.model.property.Version.VERSION_2_0);
        iCalendarProps.add(CalScale.GREGORIAN);
        iCalendarProps.add(new WrCalName(new ParameterList(), WrCalName.FACTORY, StringUtils.defaultString(applicationProperties.getDefaultBackedString(APKeys.JIRA_TITLE))));
        iCalendarProps.add(new WrCalDesc(new ParameterList(), WrCalDesc.FACTORY, StringUtils.defaultString(applicationProperties.getDefaultBackedString(APKeys.JIRA_TITLE))));
        iCalendarProps.add(new XProperty("X-WR-TIMEZONE", new ParameterList(), vTimeZone.getTimeZoneId().getValue()));

        DateTimeFormatter allDayDateValueFormatter = DateTimeFormat.forPattern("yyyyMMdd");
        StringBuilder urlBuilder = new StringBuilder();

        for (EntityAsEventService.IssueDateResult issueDateResult : result.issues)
            iCalendarComponents.add(toVEvent(allDayDateValueFormatter, urlBuilder, issueDateResult, timeZone));

        for (Version affectedVersion : result.affectedVersions)
            iCalendarComponents.add(toVEvent(allDayDateValueFormatter, urlBuilder, affectedVersion));

        for (Version fixVersion : result.fixedVersions)
            iCalendarComponents.add(toVEvent(allDayDateValueFormatter, urlBuilder, fixVersion));


        return new Calendar(iCalendarProps, iCalendarComponents);
    }

    private VEvent toVEvent(DateTimeFormatter allDayDateValueFormatter, StringBuilder urlBuilder, Version version)
            throws ParseException, URISyntaxException, MalformedURLException
    {
        PropertyList vEventProps = new PropertyList();

        urlBuilder.setLength(0);
        urlBuilder.append(String.valueOf(version.getId())).append('@').append(getHostFromBaseUrl());
        vEventProps.add(new Uid(
                new ParameterList()
                {
                    {
                        add(new XParameter(X_PARAM_UID_STATIC, Boolean.TRUE.toString()));
                    }
                },
                urlBuilder.toString()
        ));

        vEventProps.add(new Summary(version.getName()));

        DateTime releaseDate = new DateTime(version.getReleaseDate().getTime());
        vEventProps.add(new DtStart(new net.fortuna.ical4j.model.Date(allDayDateValueFormatter.print(releaseDate))));
        vEventProps.add(new DtEnd(new net.fortuna.ical4j.model.Date(allDayDateValueFormatter.print(releaseDate.plusDays(1)))));
        if (StringUtils.isNotBlank(version.getDescription()))
            vEventProps.add(new Description(version.getDescription()));

        vEventProps.add(new XProperty(X_VERSION_ID, String.valueOf(version.getId())));
        vEventProps.add(new XProperty(X_VERSION_RELEASED, String.valueOf(version.isReleased())));

        final Project versionProject = version.getProjectObject();
        vEventProps.add(new XProperty(
                X_PROJECT,
                new ParameterList()
                {
                    {
                        add(new XParameter(X_PROJECT_KEY, versionProject.getKey()));
                        add(new XParameter(X_PROJECT_ID, String.valueOf(versionProject.getId())));
                    }
                },
                versionProject.getName()
        ));

        return new VEvent(vEventProps);
    }

    private String getHostFromBaseUrl() throws MalformedURLException
    {
        return new URL(getBaseUrl()).getHost();
    }

    private String getBaseUrl()
    {
        return applicationProperties.getDefaultBackedString(APKeys.JIRA_BASEURL);
    }

    private VEvent toVEvent(DateTimeFormatter allDayDateValueFormatter, StringBuilder urlBuilder, final EntityAsEventService.IssueDateResult issueDateResult, TimeZone systemTimeZoneAsCalendarTimeZone)
            throws ParseException, URISyntaxException, MalformedURLException
    {
        PropertyList vEventProps = new PropertyList();

        urlBuilder.setLength(0);
        urlBuilder.append(issueDateResult.issueKey);

        vEventProps.add(getDateFieldVeventProperty(issueDateResult));

        if (issueDateResult instanceof EntityAsEventService.SingleDateIssueResult)
        {
            urlBuilder.append('-').append(((EntityAsEventService.SingleDateIssueResult) issueDateResult).dateFieldKey);
        }
        else if (issueDateResult instanceof EntityAsEventService.DurationIssueResult)
        {
            EntityAsEventService.DurationIssueResult durationIssueResult = (EntityAsEventService.DurationIssueResult) issueDateResult;

            urlBuilder.append('-').append(durationIssueResult.startDateFieldKey)
                    .append('-').append(durationIssueResult.endDateFieldKey);
            vEventProps.add(getEndDateFieldVeventProperty(durationIssueResult));
        }

        urlBuilder.append('@').append(getHostFromBaseUrl());

        vEventProps.add(new Uid(
                new ParameterList()
                {
                    {
                        add(new XParameter(X_PARAM_UID_STATIC, Boolean.TRUE.toString()));
                    }
                },
                urlBuilder.toString()
        ));

        vEventProps.add(new Summary(issueDateResult.issueSummary));

        if (issueDateResult.allDay)
        {
            vEventProps.add(new DtStart(new net.fortuna.ical4j.model.Date(allDayDateValueFormatter.print(issueDateResult.start))));
            vEventProps.add(new DtEnd(new net.fortuna.ical4j.model.Date(allDayDateValueFormatter.print(issueDateResult.end))));
        }
        else
        {
            DtStart dtStart = new DtStart(new net.fortuna.ical4j.model.DateTime(issueDateResult.start.toDate()));
            dtStart.setTimeZone(systemTimeZoneAsCalendarTimeZone);
            vEventProps.add(dtStart);

            DtEnd dtEnd = new DtEnd(new net.fortuna.ical4j.model.DateTime(issueDateResult.end.toDate()));
            dtEnd.setTimeZone(systemTimeZoneAsCalendarTimeZone);
            vEventProps.add(dtEnd);
        }

        if (StringUtils.isNotBlank(issueDateResult.issueDescription))
            vEventProps.add(new Description(issueDateResult.issueDescription));

        if (null != issueDateResult.assignee)
            vEventProps.add(new XProperty(
                    X_ASSIGNEE,
                    new ParameterList()
                    {
                        {
                            add(new XParameter(X_ASSIGNEE_ID, issueDateResult.assignee.getName()));
                        }
                    },
                    issueDateResult.assignee.getDisplayName()
            ));

        vEventProps.add(new DtStamp(new net.fortuna.ical4j.model.DateTime(issueDateResult.issueCreated.toDate())));
        vEventProps.add(new Created(new net.fortuna.ical4j.model.DateTime(issueDateResult.issueCreated.toDate())));
        vEventProps.add(new LastModified(new net.fortuna.ical4j.model.DateTime(issueDateResult.issueUpdated.toDate())));

        // JIRA X-properties
        vEventProps.add(new XProperty(X_ISSUE_KEY, issueDateResult.issueKey));
        vEventProps.add(new XProperty(X_ISSUE_STATUS, issueDateResult.status));
        vEventProps.add(new XProperty(X_ISSUE_RESOLUTION, issueDateResult.resolution));

        return new VEvent(vEventProps);
    }

    private Property getEndDateFieldVeventProperty(EntityAsEventService.DurationIssueResult durationIssueResult)
    {
        ParameterList propertyParams = new ParameterList();
        propertyParams.add(new XParameter(X_PARAM_ISSUE_END_DATE_FIELD_KEY, durationIssueResult.endDateFieldKey));

        return new XProperty(
                X_PROP_ISSUE_END_DATE_FIELD_NAME,
                propertyParams,
                durationIssueResult.endDateFieldName
        );
    }

    private Property getDateFieldVeventProperty(EntityAsEventService.IssueDateResult issueDateResult)
    {
        ParameterList propertyParams = new ParameterList();

        if (issueDateResult instanceof EntityAsEventService.SingleDateIssueResult)
        {
            EntityAsEventService.SingleDateIssueResult singleDateIssueResult = (EntityAsEventService.SingleDateIssueResult) issueDateResult;
            propertyParams.add(
                    new XParameter(
                            X_PARAM_ISSUE_DATE_FIELD_KEY, singleDateIssueResult.dateFieldKey
                    )
            );

            return new XProperty(
                    X_PROP_ISSUE_DATE_FIELD_NAME,
                    propertyParams,
                    singleDateIssueResult.dateFieldName
            );
        }
        else if (issueDateResult instanceof EntityAsEventService.DurationIssueResult)
        {
            EntityAsEventService.DurationIssueResult durationIssueResult = (EntityAsEventService.DurationIssueResult) issueDateResult;
            propertyParams.add(
                    new XParameter(
                            X_PARAM_ISSUE_DATE_FIELD_KEY, durationIssueResult.startDateFieldKey
                    )
            );

            return new XProperty(
                    X_PROP_ISSUE_DATE_FIELD_NAME,
                    propertyParams,
                    durationIssueResult.startDateFieldName
            );
        }

        return null;
    }

    @Path("config/fields")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getDateFields(@QueryParam("jql") String jql, @QueryParam("searchRequestId") long searchRequestId)
    {
        ApplicationUser loggedInUser = jiraAuthenticationContext.getLoggedInUser();
        Query searchQuery;

        if (0 == searchRequestId)
        {
            if (StringUtils.isBlank(jql))
                return Response.status(Response.Status.BAD_REQUEST).build();

            SearchService.ParseResult jqlParseResult = searchService.parseQuery(loggedInUser, StringUtils.defaultString(jql));

            if (!jqlParseResult.isValid())
                return Response.status(Response.Status.BAD_REQUEST).build();

            searchQuery = jqlParseResult.getQuery();
        }
        else
        {
            SearchRequest searchRequest = searchRequestService.getFilter(new JiraServiceContextImpl(loggedInUser), searchRequestId);
            if (null == searchRequest)
                return Response.status(Response.Status.BAD_REQUEST).build();

            searchQuery = searchRequest.getQuery();
        }

        Set<String> fieldKeys = new HashSet<String>(BUILT_IN_SYSTEM_FIELD_KEYS);

        Collection<CustomField> globalCustomFields = customFieldManager.getGlobalCustomFieldObjects();
        if (null != globalCustomFields)
            for (CustomField globalCustomField : Collections2.filter(globalCustomFields, new IsDateCustomFieldPredicate()))
                fieldKeys.add(globalCustomField.getId());

        Collection<Project> browseableProjectsInQuery = queryUtil.getBrowseableProjectsFromQuery(jiraAuthenticationContext.getLoggedInUser(), searchQuery);
        // Only show project level custom fields if there is one project in the query.
        if (browseableProjectsInQuery.size() == 1)
            for (Project browseableProject : browseableProjectsInQuery)
            {
                Collection<CustomField> customFieldsInProject = customFieldManager.getCustomFieldObjects(browseableProject.getId(), ConstantsManager.ALL_ISSUE_TYPES);
                if (null != customFieldsInProject)
                    for (CustomField customField : Collections2.filter(customFieldsInProject, new IsDateCustomFieldPredicate()))
                        fieldKeys.add(customField.getId());
            }

        return Response.ok(new ArrayList<DateField>(Lists.transform(
                new ArrayList<String>(fieldKeys),
                new Function<String, DateField>()
                {
                    @Override
                    public DateField apply(String fieldKey)
                    {
                        Field aField = fieldManager.getField(fieldKey);

                        DateField aDateField = new DateField();
                        aDateField.name = aField.getName();
                        aDateField.key = aField.getId();

                        return aDateField;
                    }
                }
        ))).build();
    }

    @XmlRootElement
    public static class DateField
    {
        @XmlElement
        public String key;

        @XmlElement
        public String name;
    }

    private static class IsDateCustomFieldPredicate implements Predicate<CustomField>
    {
        @Override
        public boolean apply(CustomField field)
        {
            return null != field && (
                    field.getCustomFieldType() instanceof DateTimeCFType
                            || field.getCustomFieldType() instanceof DateCFType
            );
        }
    }

    @Path("config/query/options")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getQueryOptions() throws JSONException
    {
        final ApplicationUser loggedInUser = jiraAuthenticationContext.getLoggedInUser();
        List<SimpleProject> simpleProjects = Lists.newArrayList(
                Collections2.transform(
                        Collections2.filter(
                                projectManager.getProjectObjects(),
                                Predicates.and(
                                        Predicates.notNull(),
                                        new Predicate<Project>()
                                        {
                                            @Override
                                            public boolean apply(Project project)
                                            {
                                                return permissionManager.hasPermission(Permissions.BROWSE, project, loggedInUser);
                                            }
                                        }
                                )
                        ),
                        new Function<Project, SimpleProject>()
                        {
                            @Override
                            public SimpleProject apply(Project project)
                            {
                                SimpleProject simpleProject = new SimpleProject();
                                simpleProject.key = project.getKey();
                                simpleProject.name = project.getName();

                                return simpleProject;
                            }
                        }
                )
        );
        Collections.sort(simpleProjects);

        List<SearchFilter> searchFilters = Lists.newArrayList(
                Collections2.transform(
                        getSearchFiltersForUser(loggedInUser),
                        new Function<SearchRequest, SearchFilter>()
                        {
                            @Override
                            public SearchFilter apply(SearchRequest searchRequest)
                            {
                                SearchFilter searchFilter = new SearchFilter();
                                searchFilter.id = searchRequest.getId();
                                searchFilter.name = searchRequest.getName();
                                searchFilter.description = searchRequest.getDescription();

                                return searchFilter;
                            }
                        }
                )
        );
        Collections.sort(searchFilters);

        QueryOptions queryOptions = new QueryOptions();
        queryOptions.projects = simpleProjects;
        queryOptions.searchFilters = searchFilters;

        Locale userLocale = jiraAuthenticationContext.getLocale();
        queryOptions.visibleFieldNames = autoCompleteJsonGenerator.getVisibleFieldNamesJson(loggedInUser, userLocale);
        queryOptions.visibleFunctionNamesJson = autoCompleteJsonGenerator.getVisibleFunctionNamesJson(loggedInUser, userLocale);
        queryOptions.jqlReservedWordsJson = autoCompleteJsonGenerator.getJqlReservedWordsJson();

        return Response.ok(queryOptions).build();
    }

    @Path("util/jql/validate")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response validateJql(@FormParam("jql") final String jql)
    {
        ApplicationUser loggedInUser = jiraAuthenticationContext.getLoggedInUser();
        SearchService.ParseResult jqlParseResult = searchService.parseQuery(loggedInUser, StringUtils.defaultString(jql));
        JqlValidationErrors jqlValidationErrors = new JqlValidationErrors();

        if (jqlParseResult.isValid())
        {
            return Response.ok(jqlValidationErrors).build();
        }
        else
        {
            MessageSet validationMessageSet = jqlParseResult.getErrors();

            jqlValidationErrors.errorMessages = validationMessageSet.getErrorMessages();
            jqlValidationErrors.warningMessages = validationMessageSet.getWarningMessages();

            return Response.ok(jqlValidationErrors).build();
        }
    }

    private Collection<SearchRequest> getSearchFiltersForUser(ApplicationUser loggedInUser)
    {
        return searchRequestService.search(
                new JiraServiceContextImpl(loggedInUser),
                prepareSharedEntitySearchParametersBuilder(new SharedEntitySearchParametersBuilder()).toSearchParameters(),
                0,
                Integer.MAX_VALUE
        ).getResults();
    }

    /**
     * This method is only required for backwards compatibility with JIRA 4.3, in which {@code SharedEntitySearchParametersBuilder.setEntitySearchContext} could not be found.
     *
     * @param sharedEntitySearchParametersBuilder
     * The instance of {@code SharedEntitySearchParametersBuilder} to set a search context to. Cannot be {@code null}
     * @return
     * The same instance of {@code SharedEntitySearchParametersBuilder} passed into this method.
     */
    private SharedEntitySearchParametersBuilder prepareSharedEntitySearchParametersBuilder(SharedEntitySearchParametersBuilder sharedEntitySearchParametersBuilder)
    {
        try
        {
            Class sharedEntitySearchContextClazz = getClass().getClassLoader().loadClass("com.atlassian.jira.sharing.search.SharedEntitySearchContext");
            java.lang.reflect.Field useField = sharedEntitySearchContextClazz.getField("USE");

            if (Modifier.isStatic(useField.getModifiers()))
            {
                Method setEntitySearchContextMethod = sharedEntitySearchParametersBuilder.getClass().getMethod("setEntitySearchContext", sharedEntitySearchContextClazz);
                setEntitySearchContextMethod.invoke(sharedEntitySearchParametersBuilder, useField.get(null));
            }
        }
        catch (ClassNotFoundException sharedEntitySearchContextDoesNotExist)
        {
            LOG.debug("Unable to find class SharedEntitySearchContext", sharedEntitySearchContextDoesNotExist);
        }
        catch (NoSuchFieldException useFieldNotFound)
        {
            LOG.debug("Unable to find static field \"USE\" in SharedEntitySearchContext", useFieldNotFound);
        }
        catch (NoSuchMethodException setEntitySearchContextMethodNotFound)
        {
            LOG.debug("Unable to find SharedEntitySearchParametersBuilder.setEntitySearchContext()", setEntitySearchContextMethodNotFound);
        }
        catch (IllegalAccessException unableToSetSearchContext)
        {
            LOG.debug("Invalid access to SharedEntitySearchParametersBuilder.setEntitySearchContext()", unableToSetSearchContext);
        }
        catch (InvocationTargetException setSearchContextError)
        {
            LOG.debug("Error invoking SharedEntitySearchParametersBuilder.setEntitySearchContext()", setSearchContextError);
        }

        return sharedEntitySearchParametersBuilder;
    }

    @Path("util/jql/count")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIssueCountFromJql(@FormParam("jql") String jql, @FormParam("dateFieldName") List<String> dateFieldNames, @FormParam("duration") List<String> durations, @FormParam("includeFixVersions") boolean includeFixVersions)
    {
        try
        {
            if (StringUtils.isBlank(jql))
                return Response.status(Response.Status.BAD_REQUEST).build();

            ApplicationUser loggedInUser = jiraAuthenticationContext.getLoggedInUser();
            SearchService.ParseResult jqlParseResult = searchService.parseQuery(loggedInUser, StringUtils.defaultString(jql));

            if (jqlParseResult.isValid())
            {
                if (includeFixVersions)
                {
                    dateFieldNames = new ArrayList<String>(dateFieldNames);
                    dateFieldNames.add("versiondue");
                }

                SearchResultsCount searchResultsCount = new SearchResultsCount();
                searchResultsCount.total = searchService.searchCount(
                        loggedInUser,
                        queryUtil.getQueryWithDateFieldsOptimised(
                                jqlParseResult.getQuery(),
                                dateFieldNames,
                                Collections2.transform(
                                        null == durations ? Collections.<String> emptySet() : durations,
                                        new DurationQueryParamsToDurationsTransformFunction()
                                ),
                                0,
                                0
                        )
                );

                return Response.ok(searchResultsCount).build();
            }

            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        catch (Exception generalError)
        {
            LOG.error(String.format("Unable to get the count of issues for JQL: %s[%s; %s; %s]", jql, dateFieldNames, durations, includeFixVersions), generalError);
            return Response.serverError().build();
        }
    }


    @XmlRootElement
    public static class JqlValidationErrors
    {
        @XmlElement
        public Set<String> errorMessages;

        @XmlElement
        public Set<String> warningMessages;
    }

    @XmlRootElement
    public static class QueryOptions
    {
        @XmlElement
        public List<SimpleProject> projects;

        @XmlElement
        public List<SearchFilter> searchFilters;

        @XmlElement
        public String visibleFieldNames;

        @XmlElement
        public String visibleFunctionNamesJson;

        @XmlElement
        public String jqlReservedWordsJson;

        @XmlElement
        public boolean dateRangeSupported = true;
    }

    @XmlRootElement
    public static class SimpleProject implements Comparable<SimpleProject>
    {
        @XmlElement
        public String key;

        @XmlElement
        public String name;

        @Override
        public int compareTo(SimpleProject simpleProject)
        {
            int result = StringUtils.defaultString(name).compareTo(StringUtils.defaultString(simpleProject.name));
            return 0 == result
                    ? key.compareTo(simpleProject.key)
                    : result;
        }
    }

    @XmlRootElement
    public static class SearchFilter implements Comparable<SearchFilter>
    {
        @XmlElement
        public long id;

        @XmlElement
        public String name;

        @XmlElement
        public String description;

        @Override
        public int compareTo(SearchFilter searchFilter)
        {
            int result = StringUtils.defaultString(name).compareTo(StringUtils.defaultString(searchFilter.name));
            return 0 == result
                    ? (int) (id - searchFilter.id)
                    : result;
        }
    }

    @XmlRootElement
    public static class SearchResultsCount
    {
        @XmlElement
        public long total;

        public SearchResultsCount(long total)
        {
            this.total = total;
        }

        public SearchResultsCount()
        {
            this(0);
        }
    }

    private static class DurationQueryParamsToDurationsTransformFunction implements Function<String, EntityAsEventService.Duration>
    {
        @Override
        public EntityAsEventService.Duration apply(String durationPairStr)
        {
            String[] startEndPair = StringUtils.split(durationPairStr, "/", 2);
            if (startEndPair.length == 2)
                return new EntityAsEventService.Duration(
                        startEndPair[0], startEndPair[1]
                );

            return null;
        }
    }
}
